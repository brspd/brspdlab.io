---
layout: post
title: 'Chuunibyou demo Koi ga Shitai! - Take On Me Film di CGV Cinemas'
date: 2018-05-23 19:01:12
tags:
- romansa
- remaja
- fantasi
thumbnail: https://res.cloudinary.com/antaraksi/image/upload/v1527078794/chu2_dqcjh8.png
lede: "CGV Cinemas mulai hari ini, 23 mei 2018 sudah menayangkan film anime Chuunibyou demo Koi ga Shitai! - Take On Me."
description: "Langsung saja, CGV Cinemas mulai hari ini, 23 mei 2018 sudah menayangkan film anime Chuunibyou demo Koi ga Shitai! - Take On Me. Film ini adalah cerita tambahan dari seri anime Chuunibyou demo Koi ga Shitai. Pengumuman soal film ini bisa dilihat di akun Twitter Mereka:"
---

Langsung saja, CGV Cinemas mulai hari ini, 23 mei 2018 sudah menayangkan film anime Chuunibyou demo Koi ga Shitai! - Take On Me. Pengumuman soal ini bisa dilihat di akun Twitter Mereka:


<blockquote class="twitter-tweet" data-lang="id"><p lang="in" dir="ltr">Yay! Mulai hari ini, kamu sudah bisa menyaksikan film-film seru ini lho, guys:<br><br>1. <a href="https://twitter.com/hashtag/SoloAStarWarsStory?src=hash&amp;ref_src=twsrc%5Etfw">#SoloAStarWarsStory</a><br>2. <a href="https://twitter.com/hashtag/Submergence?src=hash&amp;ref_src=twsrc%5Etfw">#Submergence</a><br>3. <a href="https://twitter.com/hashtag/3AM?src=hash&amp;ref_src=twsrc%5Etfw">#3AM</a> Part 3<br>4. <a href="https://twitter.com/hashtag/LoveChunibyoAndOtherDelusionsTheMovie?src=hash&amp;ref_src=twsrc%5Etfw">#LoveChunibyoAndOtherDelusionsTheMovie</a> : Take Me On<br><br>Buruan cek info film dan pesan tiketnya melalui <a href="https://t.co/CfGieiYldJ">https://t.co/CfGieiYldJ</a><a href="https://twitter.com/hashtag/FilmBarudiCGV?src=hash&amp;ref_src=twsrc%5Etfw">#FilmBarudiCGV</a> <a href="https://t.co/QS49uw6kAZ">pic.twitter.com/QS49uw6kAZ</a></p>&mdash; CGV Cinemas (@CGV_ID) <a href="https://twitter.com/CGV_ID/status/999248309033226240?ref_src=twsrc%5Etfw">23 Mei 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


Film ini adalah cerita tambahan dari seri anime Chuunibyou demo Koi ga Shitai!, anime drama percintaan pelajar remaja yang terjangkit Chuunibyou sindrom.


Poster dari CGV Cinemas:
![LoveChunibyoAndOtherDelusionsTheMovie](https://res.cloudinary.com/antaraksi/image/upload/v1527078437/chunnibyomovieposter_wv2ln3.jpg)

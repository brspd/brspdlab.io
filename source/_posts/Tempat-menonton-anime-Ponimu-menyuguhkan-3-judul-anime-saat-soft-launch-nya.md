---
layout: post
title: Tempat menonton anime Ponimu menyuguhkan 3 judul anime saat soft-launch-nya
date: 2018-07-31 02:22:57
tags:
 - portal anime
lede: 'Telah hadir tempat menonton anime baru. Ponimu adalah portal streaming anime pertama untuk penonton di wilayah Indonesia. Peluncuran awal Ponimu tanggal 30 juli 2018 menyuguhkan 3 judul anime.'
description: 'Telah hadir tempat menonton anime baru. Ponimu adalah portal streaming anime pertama untuk penonton di wilayah Indonesia. Peluncuran awal Ponimu tanggal 30 juli 2018 menyuguhkan 3 judul anime.'
---

Telah hadir tempat menonton anime baru, Ponimu. Ponimu adalah portal streaming anime pertama untuk penonton di wilayah Indonesia. Ponimu dapat menjadi solusi jika akses regional ditolak oleh pemberi layanan streaming anime yang berbasis di luar Indonesia. Ponimu juga bisa membuat kenikmatan menonton bertambah karena memberikan terjemahan bahasa Indonesia secara profesional.

Pada peluncuran awal Ponimu tanggal 30 juli 2018 menyuguhkan 3 judul anime. 3 judul anime tersebut terdiri dari 2 jenis, 1 judul berjenis film dan 2 judul berjenis serial tv. Berikut adalah 3 anime yang disuguhkan:
1. Project Itoh: Empire of Corpses
    Anime ini berjenis film dan bertema fiksi sains, dikeluarkan di jepang pada 2 Oktober 2015.

    <a data-pin-do="embedPin" data-pin-lang="id" data-pin-width="medium" href="https://www.pinterest.com/pin/571183165291166488/"></a>
    <script async defer src="//assets.pinterest.com/js/pinit.js"></script>

    Berikut sinopsisnya di Ponimu:
---
    Di London abad ke-19, teknologi mayat hidup telah dikembangkan sehingga orang mati dapat dipekerjakan. Seorang mahasiswa medis brilian bernama John Watson diundang bergabung dengan kelompok rahasia pemerintah UK, Institusi Walsingham, dan diberikan misi rahasia untuk mencari dokumen legendaris yang disebut “Catatan Victor”. Dr. Victor Frankenstein meninggalkan dokumen tersebut seabad lalu, dan dokumen tersebut dipercaya berisi detil teknologi rahasia untuk menciptakan “The One”, mayat hidup pertama yang dapat berbicara dan memiliki keinginan sendiri. Petunjuk pertama membawa Watson ke pedalaman Afghanistan. Alexei Karamazov, seorang pendeta militer Rusia sekaligus insinyur jenazah yang jenius, tiba-tiba memimpin sepasukan mayat bersenjata modern dan melakukan pemberontakan. Bagaimana Alexei dapat menciptakan mayat hidup tipe baru? Apakah Alexei telah mendapatkan Catatan Victor dan menggunakan metode-metode yang tercatat di dalamnya? Watson memulai petualangan besarnya mencari Catatan Victor bersama dengan seorang mayat bernama Friday.
---

2. Eden of The East
    Anime ini berjenis serial tv bertema aksi laga berbalut romansa.

    <a data-pin-do="embedPin" data-pin-lang="id" data-pin-width="medium" data-pin-terse="true" href="https://www.pinterest.com/pin/571183165291166648/"></a>

    Berikut sinopsisnya di Ponimu:
---
    Pada tanggal 22 November 2010, Jepang mendapat serangan misil yang secara aneh tidak membunuh bahkan satu orangpun. Tidak ada yang mengetahui alasan dan dari mana misil itu berasal sehingga kejadian tersebut cepat dilupakan. Tiga bulan kemudian, seorang mahasiswa bernama Saki Morimi, pergi ke Amerika dalam rangka liburan kelulusan mendapati dirinya berada dalam masalah di depan White House. Saki diselamatkan oleh seorang laki-laki bernama Akira Takizawa yang ternyata mengalami amnesia sebelum sampai di White House. Anehnya lagi, Takizawa juga membawa sebuah handphone canggih berbentuk aneh dan uang digital sebesar 8,2 miliar yen. Terlepas dari situasi yang janggal tersebut, Saki cepat menjadi dekat dengan Takizawa. Akan tetapi Saki tidak tahu bahwa ini adalah awal mula sebuah permainan mematikan yang melibatkan uang, handphone, dan penyelamatan dunia. Takizawa ternyara adalah salah satu dari 12 delegasi Jepang (Selecao) yang diberi Noblesse Phone dan ditugaskan memakai 10 triliun yen untuk menyelamatkan Jepang dengan caranya masing-masing. Saki pergi menemani Takizawa melawan 11 orang Selecao dan mengungkap rahasia Mastermind dari permainan ini, yaitu Mr. Outside. Siapakah Takizawa? Apakah Jepang dapat terselamatkan? Saksikan kisah Saki dan Takizawa selama 11 hari dalam Eden of The East
---

3. Polyphonica
    Anime ini berjenis serial tv bertema drama.

    <a data-pin-do="embedPin" data-pin-lang="id" data-pin-width="medium" data-pin-terse="true" href="https://www.pinterest.com/pin/571183165291166751/"></a>

    Berikut sinopsisnya di Ponimu:
---
    Di sebuah benua bernama Polyphonica, manusia dan roh hidup berdampingan. Phoron adalah salah satu dari seorang Dantist, yaitu pemusik yang dapat memanggil roh menggunakan musik yang disebut Commandia. Karena bakat Phoron yang tinggi, roh pasangannya adalah Corticarte Apa Lagranges yang terkenal. Corticarte terlihat seperti anak perempuan dalam bentuk manusianya, tapi dia juga memiliki julukan Crimson Annihilator. Atas perintah dari perusahaannya, Phoron dan Corticarte pergi mengelilingi benua Polyphonica menyelamatkan orang-orang dari kesulitan. Saksikan petualangan duet antara Phoron dan Corticarte yang penuh melodi dalam anime Polyphonica.
---

**Sumber:**
https://ponimu.com/home

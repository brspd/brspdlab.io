---
title: "Koleksi Baru di Situs Streaming Anime Ponimu pada Bulan September"
date: 2018-08-26 02:30:19
tags:
- portal streaming
lede: "Portal situs streaming anime Ponimu pada bulan September 2018 nanti akan mulai menayangkan koleksi anime barunya. Beberapa diantaranya adalah CANAAN, Girls und Panzer, dan TARI TARI."
description: "Portal situs streaming anime Ponimu pada bulan September 2018 nanti akan mulai menayangkan koleksi anime barunya. Beberapa diantaranya adalah CANAAN, Girls und Panzer, dan TARI TARI."
---
![Koleksi baru Ponimu di bulan september](https://res.cloudinary.com/antaraksi/image/upload/v1535230541/koleksibaruponimuseptember_x7ezox.png)

Portal situs streaming anime Ponimu pada bulan September 2018 nanti akan mulai menayangkan koleksi anime barunya. Beberapa diantaranya adalah **CANAAN**, **Girls und Panzer**, dan **TARI TARI**.

<blockquote class="twitter-tweet" data-lang="id"><p lang="in" dir="ltr">Hi kak! Tahukah kamu kalau Girl Und Panzer dan Tari Tari akan tayang di <a href="https://t.co/K1OoSRSete">https://t.co/K1OoSRSete</a> loh!!<br><br>Tak ketinggalan Canaan, Polyphonica Crimson S, dan dua movie Higashi no Eden.<br><br>Semuanya akan tayang di September, jadi jangan sampai ketinggalan serunya yah! <a href="https://t.co/MxhoddSs8J">pic.twitter.com/MxhoddSs8J</a></p>&mdash; Ponimu Indonesia (@ponimu_id) <a href="https://twitter.com/ponimu_id/status/1032983573525983232?ref_src=twsrc%5Etfw">24 Agustus 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Selain ketiga anime tersebut, Ponimu juga akan menayangkan anime baru lain namun dengan selang waktu yang berbeda. 2 Film **Eden of The East** pada pertengahan September, film **Project Itoh: Harmony** pada tanggal 3 September, dan disusul 1 film pada tanggal 29 Oktober yaitu **Project Itoh: Genocidal Organ**.

Dikutip dari Ponimu, "Untuk film "Project Itoh: Genocidal Organ", terdapat pembatasan periode holdback dari Jepang sehingga film tersebut baru bisa ditayangkan di Ponimu pada akhir bulan Oktober."

Sumber: *[Ponimu](https://www.ponimu.com/news/patch-notes---ponimu-01)*

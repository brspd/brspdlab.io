---
layout: post
title: Tsukumogami Kashimasu Akan tayang 22 Juli 2018
date: 2018-06-18 10:00:20
tags:
 - drama
 - supernatural
 - komedi
lede: 'Informasi penayangan anime TV "Tsukumogami Kashimasu" sudah diumumkan. Anime ini akan tayang mulai 22 Juli 2018 di NHK Integrated TV pukul 24:10 setiap hari Minggu. Saat pengumuman jam tayang ini, dirilis juga sebuah *key visual*.'
description: 'Informasi penayangan anime TV "Tsukumogami Kashimasu" sudah diumumkan. Anime ini akan tayang mulai 22 Juli 2018 di NHK Integrated TV pukul 24:10 setiap hari Minggu. Saat pengumuman jam tayang ini, dirilis juga sebuah key visual'
---

Informasi penayangan anime TV "Tsukumogami Kashimasu" sudah diumumkan. Anime ini akan tayang mulai 22 Juli 2018 di NHK Integrated TV pukul 24:10 setiap hari Minggu. Saat pengumuman jam tayang ini, dirilis juga sebuah *key visual*.

![Tsukumogami Kashimasu key visual](http://tsukumogami.jp/assets/img/pc/top/kv.jpg)


**Tsukumogami kashimasu** animasi TV akan punya lagu pembuka dari "Maiyavi vs Shido Kafka" dan "Mai Kuraki".

<blockquote class="twitter-tweet" data-lang="en"><p lang="ja" dir="ltr">【アーティスト情報】オープニングに「MIYAVI vs シシド・カフカ」、エンディングを「倉木麻衣」の豪華アーティストが楽曲を担当！両アーティストより意気込みコメントを頂きましたので、公式サイト&lt;スペシャル&gt;よりご覧ください！　<a href="https://twitter.com/hashtag/%E3%81%A4%E3%81%8F%E3%82%82%E3%81%8C%E3%81%BF?src=hash&amp;ref_src=twsrc%5Etfw">#つくもがみ</a><a href="https://t.co/4oVr7xilJU">https://t.co/4oVr7xilJU</a> <a href="https://t.co/EfqqKQOxCS">pic.twitter.com/EfqqKQOxCS</a></p>&mdash; アニメ『つくもがみ貸します』公式 (@tsukumogami_tv) <a href="https://twitter.com/tsukumogami_tv/status/1007428976875220992?ref_src=twsrc%5Etfw">June 15, 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


## Sinopsis

Serial ini diatur selama periode Edo, di bangsal Fukagawa dari Edo lama (Tokyo masa kini). Karena daerah tersebut rentan terhadap kebakaran dan banjir, penduduk menyewa barang-barang sehari-hari seperti pot, kasur, dan pakaian dari toko alih-alih membelinya, agar tidak menghalangi mereka ketika mereka melarikan diri.

Obeni dan Seiji, seorang kakak perempuan dan adik laki-laki, menjalankan satu toko penyewaan yang disebut Izumoya. Namun, bercampur dengan inventaris mereka adalah "tsukumogami", benda yang telah berubah menjadi roh setelah seratus tahun keberadaannya. Para saudara kandung terkadang meminjamkan barang-barang ini kepada pelanggan. Baik Obeni dan Seiji dapat melihat dan berbicara dengan roh-roh ini, dan tsukumogami lainnya sering datang ke toko setelah mendengar saudara-saudara yang terkenal.

---

Berikut video promosinya:
<iframe width="560" height="315" src="https://www.youtube.com/embed/eERHrHBGP7Q" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

*Sumber: ANN, [ota-suke](http://www.ota-suke.jp/news/219872)*

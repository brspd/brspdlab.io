---
layout: post
title: 'Animasi VP Permainan MOBA: Non-human Academy'
date: 2018-08-26 00:19:58
tags:
 - pv
lede: 'Non-human Academy video promosi dalam bentuk animasi.'
description: 'Non-human Academy video promosi dalam bentuk animasi.'
---

![Inhuman Academy](https://res.cloudinary.com/antaraksi/image/upload/v1535219459/inhumanacademy_flggz0.jpg)

**Non-human Academy** adalah permainan **MOBA** buatan **NetEase** yang kental dengan gaya anime. Berikut Non-human Academy video promosi dalam bentuk animasi:

<iframe width="560" height="315" src="https://www.youtube.com/embed/_cqJ3tCeaOw?rel=0" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>

©NetEase

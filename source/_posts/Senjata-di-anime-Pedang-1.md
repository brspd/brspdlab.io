---
layout: post
title: 'Senjata di anime: Pedang'
date: 2018-05-22 21:16:52
tags:
- senjata
- pedang
thumbnail: https://res.cloudinary.com/antaraksi/image/upload/v1527006321/aonoexorcistsword_vlajo5.jpg
lede: "Macam-macam pedang dalam anime sangat keren, kalian boleh mencontoh pedang-pedang dalam anime berikut sebagai bahan referensi."
description: "Macam-macam pedang dalam anime sangat keren, kalian boleh mencontoh pedang-pedang dalam anime berikut sebagai bahan referensi. Coba bayangkan kalian bertarung melawan makhluk jelmaan iblis dari neraka yang haus darah manusia dengan kekuatan magis yang terpendam dalam diri kalian. Seru, kan?"
---

Anime memiliki jangkauan kreatifitas cerita yang luas dan tanpa batas, hal itulah yang membuat anime dapat memuaskan fantasi-fantasi penontonnya. Coba bayangkan kalian bertarung melawan makhluk jelmaan iblis dari neraka yang haus darah manusia dengan kekuatan magis yang terpendam dalam diri kalian. Seru, kan?

Dalam pertarungan itu kalian dapat menggunakan tangan kosong dengan jurus-jurus magis, atau bisa juga menggunakan senjata. Omong-omong soal senjata, bagaimana kalau menggunakan pedang?

Macam-macam pedang dalam anime sangat keren, kalian boleh mencontoh pedang-pedang dalam anime berikut sebagai bahan referensi.

---

1. Elucidator - Sword Art Online
Elucidator adalah salah satu pedang milik Kirito dari anime Sword Art Online. Elucidator berwarna hitam pekat dengan trim abu-abu. Memiliki gagang hitam terhubung ke pelindung tangan yang jatuh di sisi kanan.
![Elucidator kirito sword](https://res.cloudinary.com/antaraksi/image/upload/v1527004564/elucidator_letteropener_c0ff7b95-142a-48d9-9b85-88e6d159f238_1024x1024_vhm40d.jpg)

2. Kurogamon - Nabari No Ou
Kurogamon adalah pedang model Jepang yang biasa disebut katana. Tuan dari Kurogamon adalah Raimei Shimizu, seorang samurai dari klan Shimizu di anime Nabari No Ou. Kurogamon tidak memiliki tambahan kekuatan magis tapi merupakan simbol penting di klan Shimizu.
![Kurogamon shimizu sword](https://res.cloudinary.com/antaraksi/image/upload/v1527005758/Nabari_no_Ou_-_02_-_Large_06_b9jfp3.jpg)

3. Kurikara - Ao No Exorcist
Kurikara adalah katana yang digunakan untuk menyegel Rin Okumura agar jiwa iblisnya tidak keluar.
![kurikara rin okumura sword](https://res.cloudinary.com/antaraksi/image/upload/v1527006321/aonoexorcistsword_vlajo5.jpg)

4. Sakabato Shinuichi - Rurouni Kenshin
Sakabato Shinuichi adalah Katana yang memiliki mata pedang terbalik. Sakabato Shinuichi digunakan oleh Himura Kenshin di anime Rurouni Kenshin. Sakabato Shinuichi tidak bisa digunakan untuk membunuh lawan karena mata pedangnya terbalik yang justru akan membahayakan penggunanya. Kalian harus ekstra hati-hati.
![Sakabato Shinuichi kenshin sword](https://res.cloudinary.com/antaraksi/image/upload/v1527006948/7806d94be934b0bd02e446df71cf1ff2494f30b1_hq_ikhpzs.jpg)

5. Blood Sword - Kyoukai No Kanata
Pedang ini adalah pedang magis dari darah Mirai Kuriyama.
![Blood Sword kuriyama Kyoukai no Kanata](https://res.cloudinary.com/antaraksi/image/upload/v1527007376/Beyond_the_Boundary_-_Mirai_Kuriyama_defends_herself_g41xvo.png)

---

Ini dulu pedang buat kalian sebagai referensi untuk melawan iblis jahat di dunia kalian. Pilih mana?

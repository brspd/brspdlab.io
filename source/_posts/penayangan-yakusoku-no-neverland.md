---
layout: post
title: 'Yakusoku No Neverland Anime Akan Tayang Januari 2019'
date: 2018-05-30 18:19:02
tags:
- misteri
thumbnail: https://res.cloudinary.com/antaraksi/image/upload/v1527680815/yokusakuthumb_ghicfb.jpg
lede: "**Yakusoku No Neverland (eng: The Promised Neverland)** anime akan tayang Januari 2019, atau winter season 2019 di **NoitaminA Fuji TV**. Anime ini akan ditayangkan setelah 'Banana Fish' selesai. Anime ini dikerjakan oleh studio yang sama dengan 'DARLING in the FRANXX' dan 'PERSONA5 The Animation', **CloverWorks**."
description: "Yakusoku No Neverland (The Promised Neverland) anime akan tayang Januari 2019, atau winter season 2019 di NoitaminA Fuji TV untuk Jepang. Anime ini akan ditayangkan setelah 'Banana Fish' selesai. Anime ini dikerjakan oleh studio yang sama dengan 'DARLING in the FRANXX' dan 'PERSONA5 The Animation', CloverWorks"
---

**Yakusoku No Neverland (eng: The Promised Neverland)** anime akan tayang Januari 2019, atau winter season 2019 di **NoitaminA Fuji TV** untuk Jepang. Anime ini akan ditayangkan setelah 'Banana Fish' selesai. Anime ini dikerjakan oleh studio yang sama dengan 'DARLING in the FRANXX' dan 'PERSONA5 The Animation', **CloverWorks**.

<div class='row'>
  <div class='col-sm12'><img alt='yakusoku no neverland' src='https://res.cloudinary.com/antaraksi/image/upload/q_auto/v1527680316/yakusoku_jbaoxk.png'></div>
</div>

**Yakusoku No Neverland** diadaptasi dari material manga yang ditulis oleh Kaiu Shirai yang diterbitkan sejak Agustus 2016 dengan ilustrasi yang digambar oleh Posuka Demizu.


Berikut video promosi yang dikutip dari akun Twitter resminya:
<div class="text-center">
  <blockquote class="twitter-tweet" data-lang="id"><p lang="ja" dir="ltr">禁断のTVアニメ化決定！！<br><br>2019年1月より フジテレビ“ノイタミナ”ほかにて放送開始！<br><br>「TVアニメ化決定PV」では、いち早くエマ・ノーマン・レイの声を聴くことができます。ぜひご覧ください！<br><br>▼アニメ公式サイト<a href="https://t.co/oxUKzp4HWv">https://t.co/oxUKzp4HWv</a><a href="https://twitter.com/hashtag/%E7%B4%84%E6%9D%9F%E3%81%AE%E3%83%8D%E3%83%90%E3%83%BC%E3%83%A9%E3%83%B3%E3%83%89?src=hash&amp;ref_src=twsrc%5Etfw">#約束のネバーランド</a> <a href="https://twitter.com/hashtag/%E7%B4%84%E3%83%8D%E3%83%90?src=hash&amp;ref_src=twsrc%5Etfw">#約ネバ</a> <a href="https://twitter.com/hashtag/%E3%83%8E%E3%82%A4%E3%82%BF%E3%83%9F%E3%83%8A?src=hash&amp;ref_src=twsrc%5Etfw">#ノイタミナ</a> <a href="https://t.co/kmLyWWd5iI">pic.twitter.com/kmLyWWd5iI</a></p>&mdash; 『約束のネバーランド』公式 (@yakuneba_staff) <a href="https://twitter.com/yakuneba_staff/status/1000753824267550720?ref_src=twsrc%5Etfw">27 Mei 2018</a></blockquote>
  <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</div>
<br>

Berikut sinopsis plot awal **Yakusoku No Neverland**:
> Hidup bahagia anak-anak panti asuhan Grace Field House harus berakhir ketika mereka mengetahui bahwa mereka dibesarkan untuk diberi makan kepada iblis. Bisakah mereka melarikan diri dari nasib mereka sebelum terlambat?

> Kehidupan di Grace Field House baik bagi Emma dan anak-anaknya. Sementara pelajaran sehari-hari dan ujian yang harus mereka ambil sulit dilakukan, pengasuh mereka yang penuh kasih memberi mereka makanan yang lezat dan banyak waktu bermain. Tapi mungkin tidak semuanya seperti yang terlihat ...

> Emma, ​​Norman, dan Ray adalah anak-anak paling cerdas di panti asuhan Grace Field House. Dan di bawah perawatan wanita yang mereka sebut sebagai "Ibu," semua anak-anak menikmati kehidupan yang nyaman. Makanan enak, pakaian bersih dan lingkungan yang sempurna untuk dipelajari — apa lagi yang bisa ditanyakan seorang orphan? Namun, suatu hari, Emma dan Norman mengungkap kebenaran gelap dari dunia luar yang dilarang mereka saksikan.


Referensi berita:
[Pengumuman anime yakusoku no neverland di Anime News Network](https://www.animenewsnetwork.com/news/2018-05-27/the-promised-neverland-manga-gets-tv-anime-in-january-2019/.131883)

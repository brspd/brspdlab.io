---
layout: post
title: '3 Anime Yang Berlatar Belakang Seperti Dunia Petualangan RPG'
date: 2018-05-28 02:27:18
tags:
- fantasi
- rpg
thumbnail: https://res.cloudinary.com/antaraksi/image/upload/c_scale,w_320/v1527456934/zesitiria_bphivv.jpg
lede: "Berikut adalah 3 anime yang bercerita tentang kehidupan di dunia antah-berantah dengan pengaturan seperti pada video game RPG. Berbeda dengan Sword Art Online atau Log Horizon, 3 anime ini tokohnya tidak benar-benar masuk kedalam video game RPG, tetapi dunia dan kehidupannya dibuat 'seperti' pada video game RPG."
description: "Berikut adalah 3 anime yang bercerita tentang kehidupan di dunia antah-berantah dengan pengaturan seperti pada video game RPG. Berbeda dengan Sword Art Online atau Log Horizon, 3 anime ini tokohnya tidak benar-benar masuk kedalam video game RPG, tetapi dunia dan kehidupannya dibuat 'seperti' pada video game RPG"
featured: true
---

Berikut adalah 3 anime yang bercerita tentang kehidupan di dunia antah-berantah dengan pengaturan seperti pada video game RPG. Berbeda dengan Sword Art Online atau Log Horizon, 3 anime ini tokohnya tidak benar-benar masuk kedalam video game RPG, tetapi dunia dan kehidupannya dibuat 'seperti' pada video game RPG.

---

1. Chain Chronicle: Haecceitas no Hikari!
Kemampuan dan kehidupan karakter-karakternya terlihat familiar seperti pada video game RPG karena Chain Chronicle merupakan anime yang diadaptasi dari mobile game RPG dengan nama yang sama.
<div class="row"><div class="col-sm-12 text-center"><img alt="chain chronicle" src="https://res.cloudinary.com/antaraksi/image/upload/c_scale,w_220/v1527455222/chainchronicle_xxo3an.jpg" style="width: auto !important"/></div></div>
<br><br>


2. Tales of Zestiria the X
Tales of Zestiria the X bercerita tentang dunia yang percaya akan keberadaan Seraphim, makhluk hidup selain manusia. di dunia yang dihuni oleh Seraphim itu teradapat banyak makhluk hidup yang berkarakter sama seperti pada video-video game.
<div class="row"><div class="col-sm-12 text-center"><img alt="chain chronicle" src="https://res.cloudinary.com/antaraksi/image/upload/c_scale,w_220/v1527456253/taleofzestiria_zlunhm.jpg" style="width: auto !important"/></div></div>
<br><br>


3. Hai to Gensou no Grimgar
Cerita tentang sekelompok pemuda-pemudi yang terdampar ke tempat yang tidak diketahui, kehidupan mereka dibuat seperti pada video game RPG, bertarung melawan monster untuk bertahan hidup.
<div class="row"><div class="col-sm-12 text-center"><img alt="chain chronicle" src="https://res.cloudinary.com/antaraksi/image/upload/c_scale,w_220/v1527456674/grimgar_tlcc0b.jpg" style="width: auto !important"/></div></div>

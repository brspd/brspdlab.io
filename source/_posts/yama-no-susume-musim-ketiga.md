---
layout: post
title: Yama No Susume Tayangan Musim Ketiga
date: 2018-06-08 14:10:04
tags:
- potongan kehidupan
lede: "Yama No Susume (US: 'Encouragement of climb') musim ketiga akan tayang di Jepang pada musim gugur. 'Premiere' pada 2 Juli di Tokyo MX."
description: "Yama No Susume (US: 'Encouragement of climb') musim ketiga akan tayang di Jepang pada musim gugur. 'Premiere' pada 2 Juli di Tokyo MX. Yama No Susume adalah anime bertema potongan kehidupan (ing: 'slice of life') dengan pemeran utama bernama Yukimura Aoi."
thumbnail: https://res.cloudinary.com/antaraksi/image/upload/c_scale,w_400/v1528444444/DfJgIHMV4AAY27E_hk6k8t.jpg
featured: true
---

Yama No Susume (US: 'Encouragement of climb') musim ketiga akan tayang di Jepang pada musim gugur. 'Premiere' pada 2 Juli di Tokyo MX, dirujuk pada berita di laman situsnya, [Yama no susume news](http://www.yamanosusume.com/news/#20180605).


Yama No Susume adalah anime bertema potongan kehidupan (ing: 'slice of life') dengan pemeran utama bernama Yukimura Aoi. Diceritakan bahwa Yukimura Aoi saat anak-anak mengagumi gunung dan berminat pada pendakian gunung, tetapi sebuah kecelakaan membuatnya takut ketinggian. Di musim ketiga ini, Yukimura Aoi berlatih untuk menaklukan gunung Fuji, gunung tertinggi di Jepang.


Berikut visual untuk musim ketiga:
<div>
<blockquote class="twitter-tweet" data-lang="id"><p lang="ja" dir="ltr"><a href="https://twitter.com/hashtag/%E3%83%A4%E3%83%9E%E3%83%8E%E3%82%B9%E3%82%B9%E3%83%A1?src=hash&amp;ref_src=twsrc%5Etfw">#ヤマノススメ</a> サードシーズン、本当のキービジュアルを公開です！あおい、ひなた、かえでさんにここなちゃん、ほのかもいる、楽しいイラストですよ～♪どこの山頂なのか、予想してみてください☆　<a href="https://t.co/j6LhQrh6Xh">https://t.co/j6LhQrh6Xh</a> <a href="https://t.co/HgBGkyb17f">pic.twitter.com/HgBGkyb17f</a></p>&mdash; 2018年7月『ヤマノススメ サードシーズン』公式 (@yamanosusume) <a href="https://twitter.com/yamanosusume/status/980460192889421826?ref_src=twsrc%5Etfw">1 April 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</div>


![Yama no susume poster dari Twitter](https://pbs.twimg.com/media/DZtLRB_VwAAqnB5.jpg)

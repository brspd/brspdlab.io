---
layout: post
title: Perkenalan Shikioriori Film Anime
date: 2018-06-11 19:32:51
tags:
 - potongan kehidupan
 - drama
thumbnail: https://res.cloudinary.com/antaraksi/image/upload/q_auto/v1528723893/shikioriorithumbnail_daoomw.png
lede: Shikioriori film adalah film anime hasil kolaborasi dari **CoMix Wave Films** yang menghasilkan film anime "Kimi no na wa." dengan **Haoliners Animation League** yang menghasilkan serial anime "Gin no Guardian".
description: 'Shikioriori film adalah film anime hasil kolaborasi dari CoMix Wave Films yang menghasilkan film anime "Kimi no na wa." dengan Haoliners Animation League yang menghasilkan serial anime "Gin no Guardian". Shikioriori memiliki 3 seri cerita yang masing-masing mengambil latar belakang di sebuah kota di China.'
featured: true
---

Shikioriori film adalah film anime hasil kolaborasi dari **CoMix Wave Films** yang menghasilkan film anime "Kimi no na wa." dengan **Haoliners Animation League** yang menghasilkan serial anime "Gin no Guardian".


Shikioriori memiliki 3 seri cerita yang masing-masing mengambil latar belakang di sebuah kota di China. Kota-kota yang dipakai adalah **Hunan**, **Guangzhou** dan **Shanghai**. Setiap seri memiliki alur cerita yang berbeda tetapi memiliki fokus tema yang sama, yaitu nostalgia dan kenangan masa muda.

**Serial Shikioriori:**
  - **Hidamari no Choshoku** (Sunny Breakfast) bercerita tentang masa kecil seorang pemuda yang bekerja di Beijing bersama neneknya.
  - **Chiisana Fashion Show** bercerita tentang perselisihan dua saudara yatim. Salah satu ingin menjadi model dan salah satu lagi ingin melanjutkan studi.
  - **Shanghai Koi** (Shanghai Love) Mengambil latar belakang Shanghai di tahun 1990. Seorang pemuda yang memendam cinta pada sahabat masa kecilnya dan tidak pernah menyatakan cintanya sampai mereka pindah tempat tinggal.


## Shikioriori visual
### Trailer

<iframe width="560" height="315" src="https://www.youtube.com/embed/GHo2Tt6wLMU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

### Film Poster dari website https://shikioriori.jp/

![Film Poster dari website https://shikioriori.jp/](https://shikioriori.jp/news/wp-content/uploads/2018/05/shiki_B1poster_fix.jpg)


### Twitter status https://twitter.com/shikioriori2018

<blockquote class="twitter-tweet" data-lang="id"><p lang="ja" dir="ltr">～『<a href="https://twitter.com/hashtag/%E8%A9%A9%E5%AD%A3%E7%B9%94%E3%80%85?src=hash&amp;ref_src=twsrc%5Etfw">#詩季織々</a>』紹介～<br><br>『詩季織々』は中国の3都市が舞台。<br>失いたくない大切な思いを胸に、大人になった若者たちの過去と今を紡いだ、3つの短編からなる珠玉の青春アンソロジーです。<a href="https://twitter.com/hashtag/%E3%81%97%E3%81%8D%E3%81%8A%E3%82%8A?src=hash&amp;ref_src=twsrc%5Etfw">#しきおり</a> <a href="https://t.co/zsTg3myltD">pic.twitter.com/zsTg3myltD</a></p>&mdash; 『詩季織々』公式 (@shikioriori2018) <a href="https://twitter.com/shikioriori2018/status/1006098747447656448?ref_src=twsrc%5Etfw">11 Juni 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


**Sumber**: *[Website Shikioriori](https://shikioriori.jp/) dan berbagai sumber.*

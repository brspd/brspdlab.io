---
layout: post
title: 'Visual Pertama Touken Ranbu Live Action Film'
date: 2018-05-31 22:43:10
tags:
- aksi
- berita
thumbnail: https://res.cloudinary.com/antaraksi/image/upload/c_scale,q_auto,w_400/v1527783055/16-9-toukenranbu3_omnpme.jpg

lede: "Touken Ranbu video game akan mendapatkan adaptasi live-action film yang akan dirilis pada tahun 2019. Film ini akan mengikut sertakan aktor-aktor yang tampil pada 'Touken Ranbu stage plays', seperti Hiroki Suzuki (Mikazuki Munechika), Yoshihiro Aramaki (Yamanbagiri Kunihiro) dan Ryo Kitamura (Yagen Toushiro)."
description: "Touken Ranbu video game akan mendapatkan adaptasi live-action film yang akan dirilis pada tahun 2019. Film ini akan mengikut sertakan aktor-aktor yang tampil pada 'Touken Ranbu stage plays', seperti Hiroki Suzuki (Mikazuki Munechika), Yoshihiro Aramaki (Yamanbagiri Kunihiro) dan Ryo Kitamura (Yagen Toushiro)."
featured: true
---

Touken Ranbu video game akan mendapatkan adaptasi live-action film yang akan dirilis pada tahun 2019. Film ini akan mengikut sertakan aktor-aktor yang tampil pada 'Touken Ranbu stage plays', seperti Hiroki Suzuki (Mikazuki Munechika), Yoshihiro Aramaki (Yamanbagiri Kunihiro) dan Ryo Kitamura (Yagen Toushiro). Film ini akan disutradarai oleh Saiji Yakumo (Ankoku Joshi) dan ditulis oleh Yasuko Kobayashi (JoJo’s Bizarre Adventure anime).

Sebelumnya, Touken Ranbu telah diadaptasi duluan kedalam anime. Ada 2 anime adaptasi, yaitu Hanamaru (2016) dan Katsugeki (2017). Berikut sedikit penjelasan tentang Hanamaru oleh **Funimation**:

---
**Hanamaru**

*Based on the hit mobile game, see Japan’s most famous swords like you’ve never seen them before—as handsome warriors!*

*In the year 2205, the past is threatened by historical revisionists who seek to change it. Protecting history becomes the task of a boisterous band of swords that are brought to life in the form of men, like Yamatonokami Yasusada and Kashuu Kiyomitsu—the two prized weapons of a Shinsengumi captain. Beyond their battles, these legendary swords and spears lead charming daily lives at a certain citadel where they hope to reunite with other weapons from the past. But Yasusada wishes for the one thing that he knows is taboo—to see his master again.*

---

http://touken-movie2019.jp/ adalah website resmi untuk film live action-nya, berikut visual pertama yang ditampilkan hari kamis 31 mei 2018:

![Touken Ranbu live action visual pertama](https://res.cloudinary.com/antaraksi/image/upload/v1527783096/1st-visual_scua3l.jpg)

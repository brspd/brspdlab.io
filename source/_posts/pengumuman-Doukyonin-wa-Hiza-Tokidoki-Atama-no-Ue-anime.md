---
layout: post
title: 'Pengumuman Doukyonin wa Hiza, Tokidoki, Atama no Ue Anime!'
date: 2018-08-24 05:14:43
tags:
 - potongan kehidupan
lede: 'Pengumuman anime Doukyonin wa Hiza, Tokidoki, Atama no Ue yang diadaptasi dari web manga Comic Polaris dan akan ditayangkan 2019. Anime ini akan ditangani oleh studio Zero-G.'
description: 'Pengumuman anime Doukyonin wa Hiza, Tokidoki, Atama no Ue yang diadaptasi dari web manga Comic Polaris dan akan ditayangkan 2019. Anime ini akan ditangani oleh studio Zero-G.'
thumbnail: https://res.cloudinary.com/antaraksi/image/upload/c_scale,q_auto:best,w_480/v1535091783/doukyounincover_qggzmr.png
og:
  image: https://res.cloudinary.com/antaraksi/image/upload/q_auto:best/v1535091783/doukyounincover_qggzmr.png
---

![Doukyonin wa Hiza, Tokidoki, Atama no Ue anime 2019](https://res.cloudinary.com/antaraksi/image/upload/v1535097726/doukyounincover2_uiw64d.png)

Web manga **Doukyonin wa Hiza, Tokidoki, Atama no Ue** yang diserialisasi oleh **Comic Polaris** mengumumkan adaptasi animenya yang akan ditayangkan 2019. Anime ini akan ditangani oleh studio **Zero-G**.

Doukyonin wa Hiza, Tokidoki, Atama no Ue adalah web manga bertema *healing/slice of life* yang berhasil masuk 20 besar '**Kono Manga ga Sugoi!**' 2017 di kategori pembaca wanita.

Doukyonin wa Hiza, Tokidoki, Atama no Ue dibuat oleh Tunami Minatuki dan Futatsuya As.

Penggemar dapat mengikuti kabar berita perkembangan animenya melalui:
- Situs resmi TV anime
https://hizaue.com/

- Akun resmi Twitter
[@hizaue_pr](https://twitter.com/hizaue_pr)

### Teaser
![Doukyonin wa Hiza, Tokidoki, Atama no Ue Poster](https://res.cloudinary.com/antaraksi/image/upload/c_scale,q_auto,w_599/v1535091806/doukyounin2_b5vnyg.jpg)

### Premis Cerita
Subaru Mikazuki adalah seorang novelis yang membenci keramaian dan lebih suka menyendiri membaca buku. Suatu hari, dia mengadopsi dan memelihara Haru, seekor kucing liar. Ceritanya akan mengambil sudut pandang kedua pihak, Subaru dan Haru.
